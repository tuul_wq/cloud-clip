module.exports = {
    lintOnSave: false,
    css: {
        loaderOptions: {
            sass: { data: `@import "~@/styles/fonts.scss";` }
            // @import "~@/styles/colors.scss"
        }
    }
}
